class Pizza:
    price = None
    description = None

    def __init__(self, decorator):
        self.decorator = decorator
    
    def __str__(self):
        return self.get_full_description()
    
    @classmethod
    def get_description(cls):
        return cls.description

    @classmethod
    def get_price(cls):
        return cls.price

    def get_full_description(self):
        return (f"{self.decorator.get_description()}'li " if self.decorator else "") \
                + self.get_description()

    def get_cost(self):
        return get_price() + \
                (self.decorator.get_price() if self.decorator else 0)

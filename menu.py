from pathlib import Path
import sys

parent_dir = Path(__file__)

def prompt_menu(pizzas, decorators):
    print_menu()
    pizza = False
    decorator = False
    while (not pizza or not decorator):
        sel = int(input("> "))
        idx = sel-1
        if idx in range(0,len(pizzas)+1):
            pizza = pizzas[idx]
        elif idx in range(10,10+len(decorators)+1):
            idx -= 10
            decorator = decorators[idx]
    return (pizza, decorator)

def print_menu():
    with parent_dir.with_name("menu.txt").open(encoding='utf-8') as f:
        print(f.read())

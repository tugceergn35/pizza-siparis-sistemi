#!/bin/sh

PROGRAM() {
	python main.py
}

CHECK_EXIT() {
	if [ $? -eq 0 ]; then
		printf "Ok.\t$1\n"
	else
		printf "Fail.\t$1\n"
	fi
}

truncate -s 0 orders.csv

PROGRAM << EOF | grep -q "Siparişiniz oluşturuldu."; CHECK_EXIT "Geçerli 2 kelimeli isim"
4
16
AAA BBB
88928668106
5511242466117883
0000
EOF

grep -q "AAA BBB" orders.csv; CHECK_EXIT "Geçerli 2 kelimeli isim kayıt"

PROGRAM << EOF | grep -q "Siparişiniz oluşturuldu."; CHECK_EXIT "Geçerli 3 kelimeli isim hatasız"
4
16
CCC DDD EEE
88928668106
5511242466117883
0000
EOF

grep -q "CCC DDD EEE" orders.csv; CHECK_EXIT "Geçerli 3 kelimeli isim kayıt edildi"

PROGRAM << EOF | grep -q "Geçersiz TC kimlik numarası."; CHECK_EXIT "Geçersiz TC kimlik numarası hatalı"
4
16
FFF GGG
79231675212
5511242466117883
0000
EOF

! grep -q "FFF GGG" orders.csv; CHECK_EXIT "Geçersiz TC kimlik numarası kayıt edilmedi"

PROGRAM << EOF | grep -q "Geçersiz isim."; CHECK_EXIT "Geçersiz 1 kelimeli isim hatalı"
4
16
HHH
79231675212
5511242466117883
0000
EOF

! grep -q "HHH" orders.csv; CHECK_EXIT "Geçersiz 1 kelimeli isim kayıt edilmedi"

PROGRAM << EOF | grep -q "Geçersiz isim."; CHECK_EXIT "Sayı içeren geçersiz isim" 
4
16
II0
79231675212
5511242466117883
0000
EOF

! grep -q "II0" orders.csv; CHECK_EXIT "Sayı içeren geçersiz isim kayıt edilmedi"

### Visa (16)
PROGRAM << EOF | grep -q "Siparişiniz oluşturuldu."; CHECK_EXIT "Geçerli 16 haneli Visa kartı"
4
16
JJJ KKK
96958275348
4929160365772476
0000
EOF

grep -q "JJJ KKK" orders.csv; CHECK_EXIT "Geçerli 16 haneli Visa kartı kayıt edildi"

### Visa (13)
PROGRAM << EOF | grep -q "Siparişiniz oluşturuldu."; CHECK_EXIT "Geçerli 13 haneli Visa kartı"
4
16
LLL MMM
19269195754
4539886236762
0000
EOF

grep -q "LLL MMM" orders.csv; CHECK_EXIT "Geçerli 13 haneli Visa kartı kayıt edildi"

### MasterCard
PROGRAM << EOF | grep -q "Siparişiniz oluşturuldu."; CHECK_EXIT "Geçerli MasterCard kartı"
4
16
NNN OOO
43296221100
5352435546827669
0000
EOF

grep -q "NNN OOO" orders.csv; CHECK_EXIT "Geçerli MasterCard kartı kayıt edildi"

### Geçersiz Visa (16)
PROGRAM << EOF | grep -q "Geçersiz kart numarası."; CHECK_EXIT "Geçersiz 16 haneli Visa kartı"
4
16
PPP QQQ
52121196568
4536578866310137
0000
EOF

! grep -q "PPP QQQ" orders.csv; CHECK_EXIT "Geçersiz 16 haneli Visa kartı kayıt edilmedi"

### Geçersiz Visa (13)
PROGRAM << EOF | grep -q "Geçersiz kart numarası."; CHECK_EXIT "Geçersiz 13 haneli Visa kartı"
4
16
RRR SSS
19269195754
4574947559234
0000
EOF

! grep -q "RRR SSS" orders.csv; CHECK_EXIT "Geçersiz 13 haneli Visa kartı kayıt edilmedi"

### Geçersiz MasterCard
PROGRAM << EOF | grep -q "Geçersiz kart numarası."; CHECK_EXIT "Geçersiz MasterCard kartı"
4
16
TTT UUU
28974638456
5352435547827669
0000
EOF

! grep -q "TTT UUU" orders.csv; CHECK_EXIT "Geçersiz MasterCard kartı kayıt edilmedi"

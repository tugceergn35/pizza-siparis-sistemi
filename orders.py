from pathlib import Path
from datetime import datetime
import csv

parent_dir = Path(__file__)

def add(order, customer):
    with parent_dir.with_name("orders.csv").open(mode='a', encoding='utf-8') as orders_file:
        orders_writer = csv.writer(orders_file, delimiter=',', quotechar="'", quoting=csv.QUOTE_MINIMAL)
        orders_writer.writerow([customer.get_name(), customer.get_id_no(), customer.get_card_no(), customer.get_card_pass(), datetime.now().isoformat()])

    return True

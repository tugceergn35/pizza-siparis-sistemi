### Üçlü kontrol mekanizması (ternary operator) içerisinde hata çıkartamadığımız için (https://stackoverflow.com/q/54824572) yardımcı bir fonksiyon kullanıyoruz.
def _raise(e):
    raise e

class Customer:
    def __init__(self, name, id_no, card_no, card_pass):
        self.name = name if Customer.verify_name(name) else _raise(ValueError("Geçersiz isim."))
        self.id_no = id_no if Customer.verify_id_no(id_no) else _raise(ValueError("Geçersiz TC kimlik numarası."))
        self.card_no = card_no if Customer.verify_card_no(card_no) else _raise(ValueError("Geçersiz kart numarası."))
        self.card_pass = card_pass if Customer.verify_card_pass(card_pass) else _raise(ValueError("Geçersiz kart şifresiz."))

    @staticmethod
    def verify_name(name):
        if (len(name.split()) < 2):
            return False

        if any(c.isdigit() for c in name):
            return False

        return True

    @staticmethod
    def verify_card_pass(card_pass):
        return len(str(card_pass)) == 4

    ### Kredi kartı numarası kontrol fonksiyonu
    ### Referans: https://en.wikipedia.org/wiki/Luhn_algorithm
    @staticmethod
    def verify_card_no(card_no):
        s = 0
        double_digit = False
        card_no_str = str(card_no)
        for i in reversed(range(0, len(card_no_str))):
            digit = int(card_no_str[i])
            if double_digit:
                digit *= 2
                if digit > 9:
                    digit -= 9
            s += digit
            double_digit = not double_digit

        if s % 10 != 0:
            return False

        # Visa
        if len(card_no_str) in [13, 16] and int(card_no_str[0]) == 4:
            return True

        # MasterCard
        if len(card_no_str) == 16 and int(card_no_str[0:2]) in range(51, 56):
            return True

        return False

    ### Kimlik kartı numarası kontrol fonksiyonu
    ### Referans: https://www.yusufsezer.com.tr/javascript-tc-kimlik-no-dogrulama/
    @staticmethod
    def verify_id_no(id_no):
        odd = 0
        even = 0
        result = 0
        s = 0

        if id_no != id_no:
            return False

        id_no_str = str(id_no)

        if len(id_no_str) != 11:
            return False

        if id_no in [11111111110, 22222222220, 33333333330, 44444444440, 55555555550, 66666666660, 7777777770, 88888888880, 99999999990]:
            return False

        for i in range(0, 9, 2):
            odd += int(id_no_str[i])

        for i in range(1, 8, 2):
            even += int(id_no_str[i])

        odd = odd * 7
        result = abs(odd - even)
        if result % 10 != int(id_no_str[9]):
            return False

        for i in range(0, 10): 
            s += int(id_no_str[i])

        if s % 10 != int(id_no_str[10]):
            return False

        return True

    def get_name(self):
        return self.name

    def get_id_no(self):
        return self.id_no

    def get_card_no(self):
        return self.card_no

    def get_card_pass(self):
        return self.card_pass

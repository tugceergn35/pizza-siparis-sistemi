import menu
import orders

from Customer import Customer
from pizzas import *
from decorators import *

pizzas = [Klasik, Margarita, TurkPizza, Sade]
decorators = [Zeytin, Mantarlar, KeciPeyniri, Et, Sogan, Misir]

def main():
    (pizza, decorator) = menu.prompt_menu(pizzas, decorators)
    order = pizza(decorator)
    print(order)

    name = str(input("İsminiz: "))
    id_no = int(input("TC kimlik numaranız: "))
    card_no = int(input("Kredi kartı numaranız: "))
    card_pass = str(input("Kredi kartı şifreniz: "))

    customer = None
    try:
        customer = Customer(name, id_no, card_no, card_pass)
    except ValueError as e:
        print(e)
        return False

    if orders.add(order, customer):
        print("Siparişiniz oluşturuldu.")
    else:
        print("Siparişiniz oluşturulamadı.")
        return False

    return True

if __name__ == "__main__":
    main()

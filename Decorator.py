class Decorator:
    price = None
    description = None

    @classmethod
    def get_description(cls):
        return cls.description

    @classmethod
    def get_price(cls):
        return cls.price
